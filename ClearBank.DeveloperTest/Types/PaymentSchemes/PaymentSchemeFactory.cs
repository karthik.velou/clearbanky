﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClearBank.DeveloperTest.Types.PaymentSchemes
{
    public class PaymentSchemeFactory : IPaymentSchemeFactory
    {
        public IPaymentScheme Create(PaymentScheme paymentScheme)
        {
            IPaymentScheme scheme = null;
            switch (paymentScheme)
            {
                case PaymentScheme.Bacs: 
                    scheme = new BacsPaymentScheme(); 
                    break;
                case PaymentScheme.FasterPayments:
                    scheme = new FasterPaymentsScheme();
                    break ;
                case PaymentScheme.Chaps:
                    scheme = new ChapsPaymentScheme();
                    break;
            }
            return scheme;
        }
    }
}

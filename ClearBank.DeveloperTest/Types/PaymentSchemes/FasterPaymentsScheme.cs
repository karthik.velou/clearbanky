﻿namespace ClearBank.DeveloperTest.Types.PaymentSchemes
{
    public class FasterPaymentsScheme : IPaymentScheme
    {
        public bool IsValid(Account account, MakePaymentRequest request)
        {
            bool isValid = true;
            if (account == null)
            {
                isValid = false;
            }
            else if (!account.AllowedPaymentSchemes.HasFlag(AllowedPaymentSchemes.FasterPayments))
            {
                isValid = false;
            }
            else if (account.Balance < request.Amount)
            {
                isValid = false;
            }
            return isValid;
        }
    }
}

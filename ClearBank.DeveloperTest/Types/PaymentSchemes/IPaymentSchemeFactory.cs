﻿namespace ClearBank.DeveloperTest.Types.PaymentSchemes
{
    public interface IPaymentSchemeFactory
    {
        IPaymentScheme Create(PaymentScheme paymentScheme);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClearBank.DeveloperTest.Types.PaymentSchemes
{
    public interface IPaymentScheme
    {
        public bool IsValid(Account account, MakePaymentRequest request);
    }
}

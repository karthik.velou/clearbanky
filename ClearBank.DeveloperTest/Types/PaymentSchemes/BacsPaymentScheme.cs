﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClearBank.DeveloperTest.Types.PaymentSchemes
{
    internal class BacsPaymentScheme : IPaymentScheme
    {
        public bool IsValid(Account account, MakePaymentRequest request)
        {
            bool isValid = true;
            if (account == null)
            {
                isValid = false;
            }
            else if (!account.AllowedPaymentSchemes.HasFlag(AllowedPaymentSchemes.Bacs))
            {
                isValid = false;
            }
            return isValid;
        }
    }
}

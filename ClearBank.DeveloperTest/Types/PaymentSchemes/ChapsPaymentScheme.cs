﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClearBank.DeveloperTest.Types.PaymentSchemes
{
    public class ChapsPaymentScheme : IPaymentScheme
    {
        public bool IsValid(Account account, MakePaymentRequest request)
        {
            bool isValid = true;
            if (account == null)
            {
                isValid = false;
            }
            else if (!account.AllowedPaymentSchemes.HasFlag(AllowedPaymentSchemes.Chaps))
            {
                isValid = false;
            }
            else if (account.Status != AccountStatus.Live)
            {
                isValid = false;
            }
            return isValid;
        }
    }
}

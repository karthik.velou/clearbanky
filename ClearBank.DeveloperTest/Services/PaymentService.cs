﻿using ClearBank.DeveloperTest.Config;
using ClearBank.DeveloperTest.Data;
using ClearBank.DeveloperTest.Types;
using ClearBank.DeveloperTest.Types.PaymentSchemes;
using System;

namespace ClearBank.DeveloperTest.Services
{
    public class PaymentService : IPaymentService
    {
        private readonly IDataStoreFactory _dataStoreFactory;
        private readonly IPaymentSchemeFactory _paymentSchemeFactory;
        private readonly IDataStoreConfig _dataStoreConfig;

        //ToDo: PaymentService(IDataStore dataStore, IPaymentSchemeFactory paymentSchemeFactory)
        public PaymentService(IDataStoreConfig dataStoreConfig, IDataStoreFactory dataStoreFactory, IPaymentSchemeFactory paymentSchemeFactory)
        {
            _dataStoreFactory = dataStoreFactory;
            _paymentSchemeFactory = paymentSchemeFactory;
            _dataStoreConfig = dataStoreConfig;
        }

        public MakePaymentResult MakePayment(MakePaymentRequest request)
        {
            var result = new MakePaymentResult();

            try
            {
                var accountDataStore = _dataStoreFactory.Create(_dataStoreConfig.DataStore);
                if (accountDataStore == null)
                {
                    //_logger.Error($"Data Store is null for {_dataStoreConfig.DataStore}")
                    result.Success = false;
                    return result;
                }
                
                var paymentScheme = _paymentSchemeFactory.Create(request.PaymentScheme);
                if (paymentScheme == null)
                {
                    //_logger.Error($"Payment Scheme is null for {request.PaymentScheme}")
                    result.Success = false;
                    return result;
                }

                var account = accountDataStore.GetAccount(request.DebtorAccountNumber);
                result.Success = paymentScheme.IsValid(account, request);

                if (result.Success)
                {
                    account.Balance -= request.Amount;
                    accountDataStore.UpdateAccount(account);
                }
            }
            catch (Exception)
            {
                //log
            }

            return result;
        }
    }
}

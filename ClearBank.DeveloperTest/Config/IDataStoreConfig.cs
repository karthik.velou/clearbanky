﻿using ClearBank.DeveloperTest.Types;

namespace ClearBank.DeveloperTest.Config
{
    public interface IDataStoreConfig
    {
        DataStoreType? DataStore { get; }
    }
}

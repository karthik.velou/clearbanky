﻿using ClearBank.DeveloperTest.Types;
using System;
using System.Configuration;

namespace ClearBank.DeveloperTest.Config
{
    public class DataStoreConfig : IDataStoreConfig
    {
        public DataStoreType? DataStore => GetConfigValue();

        private DataStoreType? GetConfigValue()
        {
            return Enum.TryParse(ConfigurationManager.AppSettings["DataStoreType"], out DataStoreType dataStore) 
                ? dataStore 
                : (DataStoreType?)null;
        }
    }
}

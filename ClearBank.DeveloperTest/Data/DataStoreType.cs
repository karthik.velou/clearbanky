﻿namespace ClearBank.DeveloperTest.Types
{
    public enum DataStoreType
    {
        Account,
        Backup
    }
}
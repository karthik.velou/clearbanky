﻿using ClearBank.DeveloperTest.Types;

namespace ClearBank.DeveloperTest.Data
{
    public class DataStoreFactory : IDataStoreFactory
    {
        public IDataStore Create(DataStoreType? dataStore)
        {
            IDataStore store = null;
            switch (dataStore)
            {
                case DataStoreType.Account: 
                    store = new AccountDataStore(); 
                    break;
                case DataStoreType.Backup: 
                    store = new BackupAccountDataStore(); 
                    break;
            }
            return store;
        }
    }
}

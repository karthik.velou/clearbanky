﻿using ClearBank.DeveloperTest.Data;
using ClearBank.DeveloperTest.Services;
using ClearBank.DeveloperTest.Types;
using ClearBank.DeveloperTest.Types.PaymentSchemes;
using ClearBank.DeveloperTest.Config;
using Moq;
using Xunit;

namespace ClearBank.DeveloperTest.Tests
{
    public class PaymentServiceTests
    {
        private readonly PaymentService _paymentService;
        private MakePaymentRequest _request;

        private readonly Mock<IDataStore> _mockDataStore = new Mock<IDataStore>();
        private readonly Mock<IDataStoreConfig> _mockDataStoreConfig = new Mock<IDataStoreConfig>();        
        private readonly Mock<IDataStoreFactory> _mockDataStoreFactory = new Mock<IDataStoreFactory>();        

        public PaymentServiceTests()
        {
            _paymentService = new PaymentService(_mockDataStoreConfig.Object, _mockDataStoreFactory.Object, new PaymentSchemeFactory());
        }

        [Theory]
        [InlineData(PaymentScheme.Bacs)]
        [InlineData(PaymentScheme.FasterPayments)]
        [InlineData(PaymentScheme.Chaps)]
        public void Account_Null_Test(PaymentScheme paymentScheme)
        {
            //Arrange
            _request = new MakePaymentRequest
            {
                PaymentScheme = paymentScheme,
                DebtorAccountNumber = "123456"
            };
            _mockDataStoreConfig.Setup(x => x.DataStore).Returns(DataStoreType.Account);
            _mockDataStore.Setup(x => x.GetAccount(It.IsAny<string>())).Returns((Account)null);
            _mockDataStoreFactory.Setup(x => x.Create(_mockDataStoreConfig.Object.DataStore)).Returns(_mockDataStore.Object);

            //Act
            var result = _paymentService.MakePayment(_request);

            //Assert
            Assert.False(result.Success);
        }

        [Theory]
        [InlineData(PaymentScheme.Chaps, AllowedPaymentSchemes.Chaps | AllowedPaymentSchemes.Bacs | AllowedPaymentSchemes.FasterPayments)]
        [InlineData(PaymentScheme.Bacs, AllowedPaymentSchemes.Chaps | AllowedPaymentSchemes.Bacs | AllowedPaymentSchemes.FasterPayments)]
        [InlineData(PaymentScheme.FasterPayments, AllowedPaymentSchemes.Chaps | AllowedPaymentSchemes.Bacs | AllowedPaymentSchemes.FasterPayments)]
        public void PaymentSchemes_Valid_Test(PaymentScheme paymentScheme, AllowedPaymentSchemes allowedPaymentSchemes)
        {
            //Arrange
            _request = new MakePaymentRequest
            {
                PaymentScheme = paymentScheme,
                DebtorAccountNumber = "123456"
            };
            _mockDataStoreConfig.Setup(x => x.DataStore).Returns(DataStoreType.Account);
            _mockDataStore.Setup(x => x.GetAccount(It.IsAny<string>())).Returns(new Account { AllowedPaymentSchemes = allowedPaymentSchemes });
            _mockDataStore.Setup(x => x.UpdateAccount(It.IsAny<Account>()));
            _mockDataStoreFactory.Setup(x => x.Create(_mockDataStoreConfig.Object.DataStore)).Returns(_mockDataStore.Object);

            //Act
            var result = _paymentService.MakePayment(_request);

            //Assert
            Assert.True(result.Success);
        }

        [Theory]
        [InlineData(PaymentScheme.Chaps, AllowedPaymentSchemes.Bacs | AllowedPaymentSchemes.FasterPayments)]
        [InlineData(PaymentScheme.Bacs, AllowedPaymentSchemes.FasterPayments | AllowedPaymentSchemes.Chaps)]
        [InlineData(PaymentScheme.FasterPayments, AllowedPaymentSchemes.Chaps | AllowedPaymentSchemes.Bacs)]
        public void PaymentSchemes_Invalid_Test(PaymentScheme paymentScheme, AllowedPaymentSchemes allowedPaymentSchemes)
        {
            //Arrange
            _request = new MakePaymentRequest
            {
                PaymentScheme = paymentScheme,
                DebtorAccountNumber = "123456"
            };
            _mockDataStoreConfig.Setup(x => x.DataStore).Returns(DataStoreType.Account);
            _mockDataStore.Setup(x => x.GetAccount(It.IsAny<string>())).Returns(new Account { AllowedPaymentSchemes = allowedPaymentSchemes });
            _mockDataStore.Setup(x => x.UpdateAccount(It.IsAny<Account>()));
            _mockDataStoreFactory.Setup(x => x.Create(_mockDataStoreConfig.Object.DataStore)).Returns(_mockDataStore.Object);

            //Act
            var result = _paymentService.MakePayment(_request);

            //Assert
            Assert.False(result.Success);
        }

        [Theory]
        [InlineData(PaymentScheme.FasterPayments, AllowedPaymentSchemes.FasterPayments)]
        public void AccountBalance_Invalid_Test(PaymentScheme paymentScheme, AllowedPaymentSchemes allowedPaymentSchemes)
        {
            //Arrange
            _request = new MakePaymentRequest
            {
                PaymentScheme = paymentScheme,
                DebtorAccountNumber = "123456",
                Amount = 100.00m
            };
            _mockDataStoreConfig.Setup(x => x.DataStore).Returns(DataStoreType.Account);
            _mockDataStore.Setup(x => x.GetAccount(It.IsAny<string>())).Returns(new Account
            {
                AllowedPaymentSchemes = allowedPaymentSchemes,
                Balance = 99.99m
            });
            _mockDataStore.Setup(x => x.UpdateAccount(It.IsAny<Account>()));
            _mockDataStoreFactory.Setup(x => x.Create(_mockDataStoreConfig.Object.DataStore)).Returns(_mockDataStore.Object);

            //Act
            var result = _paymentService.MakePayment(_request);

            //Assert
            Assert.False(result.Success);
        }

        [Theory]
        [InlineData(PaymentScheme.FasterPayments, AllowedPaymentSchemes.FasterPayments)]
        public void AccountBalance_Valid_Test(PaymentScheme paymentScheme, AllowedPaymentSchemes allowedPaymentSchemes)
        {
            //Arrange
            _request = new MakePaymentRequest
            {
                PaymentScheme = paymentScheme,
                DebtorAccountNumber = "123456",
                Amount = 100.00m
            };
            _mockDataStoreConfig.Setup(x => x.DataStore).Returns(DataStoreType.Account);            
            _mockDataStore.Setup(x => x.GetAccount(It.IsAny<string>())).Returns(new Account
            {
                AllowedPaymentSchemes = allowedPaymentSchemes,
                Balance = 100.01m
            });
            _mockDataStore.Setup(x => x.UpdateAccount(It.IsAny<Account>()));
            _mockDataStoreFactory.Setup(x => x.Create(_mockDataStoreConfig.Object.DataStore)).Returns(_mockDataStore.Object);

            //Act
            var result = _paymentService.MakePayment(_request);

            //Assert
            Assert.True(result.Success);
        }

        [Theory]
        [InlineData(PaymentScheme.Chaps, AllowedPaymentSchemes.Chaps)]
        public void AccountStatus_Invalid_Test(PaymentScheme paymentScheme, AllowedPaymentSchemes allowedPaymentSchemes)
        {
            //Arrange
            _request = new MakePaymentRequest
            {
                PaymentScheme = paymentScheme,
                DebtorAccountNumber = "123456",
            };
            _mockDataStoreConfig.Setup(x => x.DataStore).Returns(DataStoreType.Account);
            _mockDataStore.Setup(x => x.GetAccount(It.IsAny<string>())).Returns(new Account
            {
                AllowedPaymentSchemes = allowedPaymentSchemes,
                Status = AccountStatus.Disabled
            });
            _mockDataStore.Setup(x => x.UpdateAccount(It.IsAny<Account>()));
            _mockDataStoreFactory.Setup(x => x.Create(_mockDataStoreConfig.Object.DataStore)).Returns(_mockDataStore.Object);

            //Act
            var result = _paymentService.MakePayment(_request);

            //Assert
            Assert.False(result.Success);
        }

        [Theory]
        [InlineData(PaymentScheme.Chaps, AllowedPaymentSchemes.Chaps)]
        public void AccountStatus_Valid_Test(PaymentScheme paymentScheme, AllowedPaymentSchemes allowedPaymentSchemes)
        {
            //Arrange
            _request = new MakePaymentRequest
            {
                PaymentScheme = paymentScheme,
                DebtorAccountNumber = "123456",
            };
            _mockDataStoreConfig.Setup(x => x.DataStore).Returns(DataStoreType.Account);
            _mockDataStore.Setup(x => x.GetAccount(It.IsAny<string>())).Returns(new Account
            {
                AllowedPaymentSchemes = allowedPaymentSchemes,
                Status = AccountStatus.Live
            });
            _mockDataStore.Setup(x => x.UpdateAccount(It.IsAny<Account>()));
            _mockDataStoreFactory.Setup(x => x.Create(_mockDataStoreConfig.Object.DataStore)).Returns(_mockDataStore.Object);

            //Act
            var result = _paymentService.MakePayment(_request);

            //Assert
            Assert.True(result.Success);
        }
    }
}
